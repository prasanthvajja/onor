from rest_framework import serializers
from onorapp.models import MainPageCarosuel

class MainPageCarosuelSerializers(serializers.ModelSerializer):
    class Meta:
        model=MainPageCarosuel
        fields="__all__"
