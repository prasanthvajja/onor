from rest_framework import serializers
from onorapp.models import CategoryCarosuel

class CategoryCarosuelSerializers(serializers.ModelSerializer):
    class Meta:
        model=CategoryCarosuel
        fields="__all__"
