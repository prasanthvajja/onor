from rest_framework import serializers
from onorapp.models import Carosuel

class CarosuelSerializers(serializers.ModelSerializer):
    class Meta:
        model=Carosuel
        fields="__all__"
