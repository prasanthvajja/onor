from django.conf.urls import include,url
from onorapp.Controllers.Roles import addRoles,getUpdateRoles
from onorapp.Controllers.User import getUpdateDeleteUsers,addUsers
from onorapp.Controllers.MainPageCarosuel import addMainPageCarosuel,getUpdateDeleteMainPageCarosuel
from onorapp.Controllers.Carosuel import addCarosuel,getUpdateDeleteCarosuel
from onorapp.Controllers.Categories import addCategories,getUpdateDeleteCategories
from onorapp.Controllers.CategoryCarosuel import addCategoaryCarosuel,getUpdateDeleteCategoryCarosuel
from onorapp.Controllers.Categorylist import addCategoryList,getUpdateDeleteCatagoryList
from onorapp.Controllers.Categorylistimages import addCategoryListImages, getUpdateDeleteCatagoryListImages


urlpatterns=[
     url(r'^getupdateroles/(?P<id>[0-99]+)/$',getUpdateRoles.as_view()),
     url(r'^addroles/',addRoles.as_view()),
     url(r'^users/(?P<id>[0-99]+)/$',getUpdateDeleteUsers.as_view()),
     url(r'^addUsers/',addUsers.as_view()),
     url(r'^mainpagecarosuel/(?P<id>[0-99]+)/$',getUpdateDeleteMainPageCarosuel.as_view()),
     url(r'^addmainpagecarosuel/',addMainPageCarosuel.as_view()),
     url(r'^carosuel/(?P<id>[0-99]+)/$',getUpdateDeleteCarosuel.as_view()),
     url(r'^addcarosuel/',addCarosuel.as_view()),
     url(r'^categories/(?P<id>[0-99]+)/$',getUpdateDeleteCategories.as_view()),
     url(r'^addcatagories/',addCategories.as_view()),
     url(r'^getUpdateDeleteCategoryCarosuel/(?P<id>[0-99]+)/$',getUpdateDeleteCategoryCarosuel.as_view()),
     url(r'^addCategoaryCarosuel/',addCategoaryCarosuel.as_view()),
     url(r'^getUpdateDeleteCatagoryList/(?P<id>[0-99]+)/$',getUpdateDeleteCatagoryList.as_view()),
     url(r'^addCategoryList/',addCategoryList.as_view()),
     url(r'^getUpdateDeleteCatagoryListImages/(?P<id>[0-99]+)/$',getUpdateDeleteCatagoryListImages.as_view()),
     url(r'^addCategoryListImages/',addCategoryListImages.as_view()),
]
