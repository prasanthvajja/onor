from django.contrib import admin
from .models import Roles,Users,Categories,MainPageCarosuel,Carosuel,CategoryCarosuel,CategoryList,CategoryListImages

class RolesDetails(admin.ModelAdmin):
    list_display=('name','status')
admin.site.register(Roles,RolesDetails)

class UserDetails(admin.ModelAdmin):
    list_display=('role','first_name','last_name','mobile_no','address','emailId','password','status')
admin.site.register(Users,UserDetails)

class MainPageCarosuelDetails(admin.ModelAdmin):
    list_display=('carosuelname','user','status')
admin.site.register(MainPageCarosuel,MainPageCarosuelDetails)

class CarosuelDetails(admin.ModelAdmin):
    list_display=('slide','user','status')
admin.site.register(Carosuel,CarosuelDetails)

class CategoryCarosuelDetails(admin.ModelAdmin):
    list_display=('category','slide','user','status')
admin.site.register(CategoryCarosuel,CategoryCarosuelDetails)

class CategoriesDetails(admin.ModelAdmin):
    list_display=('category','image','user','status')
admin.site.register(Categories,CategoriesDetails)

class CategoryListDetails(admin.ModelAdmin):
    list_display=('name','mobile','email','description','images','category','user','status')
    search_fields=('category',)
admin.site.register(CategoryList,CategoryListDetails)

class CategoryListImagesDetails(admin.ModelAdmin):
    list_display=('list','images','user','status')
admin.site.register(CategoryListImages,CategoryListImagesDetails)
